/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_line2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftothmur <ftothmur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/20 19:50:31 by ftothmur          #+#    #+#             */
/*   Updated: 2019/11/21 12:38:01 by ftothmur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int					initialize(t_gl *gl, int fd, char **line)
{
	if (fd < 0 || !line || BUFF_SIZE <= 0 || BUFF_SIZE > 8388608 ||
			read(fd, NULL, 0) == FAILURE)
	{
		ft_bzero();
		return (FAILURE);
	}
	gl->reader.fd = fd;
	gl->reader.line = line;
	*gl->reader.line = NULL;
	gl->reader.line_len = 0;
	return (SUCCESS);
}

void				read_to_buffer(t_gl *gl)
{
	gl->load = read(gl->reader->fd, (void *)gl->buffer, BUFF_SIZE);

	return ;
}

int					ft_gnl_bald(int fd, char **line)
{
	static t_gl		gl;
	static t_gl_ops	dispatcher[GL_OPERATIONS] =
	{
					read_to_buffer
	};

	if (initialize(&gl, fd, line))
		return (FAILURE);
	while (TRUE)
		(*dispatcher[gl.state])();
	return (buf.line_len);
}