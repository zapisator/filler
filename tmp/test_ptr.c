/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_ptr.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftothmur <ftothmur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/11 16:58:12 by ftothmur          #+#    #+#             */
/*   Updated: 2019/11/12 12:51:03 by ftothmur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int                 main(void)
{
    char            *line1;
    char            *line2;

    line1 = NULL;
    line2 = line1;
    !*line1 : (line2 = "Line2 was changed.\n");
    printf("%s", !line2 ? "NULL" : line2);
    return (0);
}